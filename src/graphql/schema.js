import { makeExecutableSchema } from "graphql-tools";

import Mutations from "./mutations";
import Queries from "./queries";
import resolvers from "./resolvers/";

const typeDefs = `
  type User {
    id: Int!
    name: String!
  }

  ${ Queries }
  ${ Mutations }
`

export {
  typeDefs,
  resolvers
}
